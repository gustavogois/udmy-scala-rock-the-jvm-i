package lectures.part1basics

object Expressions extends App {

  val x = 1 + 2 // Expression
  println(x)

  println(1 == x)

  // Instructions (tells something to the computer to do) vs Expressions (has a value)
  // Instructions are executed (think Java), expressions are evaluated (Scala)
  // In Scala we'll think in terms of expressions

  // IF expression
  val aCondition = true
  val aConditionedValue = if(aCondition) 5 else 3
  println(aConditionedValue)
  println(if(aCondition) 5 else 3)


  // We discourage loops in Scala. They are very specific to imperative language programs.
  // They don't really return anything meaningful and only executes side effects.
  var i = 0
  while(i < 10) {
    println(i)
    i += 1
  }
  // NEVER WRITE THIS AGAIN !! The single worst thing a scala programmer can do really is write imperative code
  // with Scala syntax

  // EVERTHING (operations, calling functions, if expressions, etc) in Scala is an Expression!

  var aVariable = 2
  val aWeirdValue = (aVariable = 3)   // Unit === void. Just means don't return anything meaningful
  println(aWeirdValue)   // => ()

  val aWhile = (while(i < 10) {
    println(i)
    i += 1
  })               // => Unit
  // side effects (are reminiscent of imperative programming): println(), whiles, reassigning



  // Code blocks: are a special kind of expressions because they have some special properties

  val aCodeBlock = {
    val y = 2
    val z = y + 1
    if (z > 2) "hello" else "goodbye"
  }   // One code block is an expression too. And the block value is the value of its last expression


}
