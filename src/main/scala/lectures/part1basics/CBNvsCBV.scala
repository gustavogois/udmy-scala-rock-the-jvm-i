package lectures.part1basics

object CBNvsCBV extends App {

  def calledByValue(x: Long): Unit = {
    println("by value: " + x)
    println("by value: " + x)
  }

  def calledByName(x: => Long): Unit = {  // the expression is evaluated every time
    println("by value: " + x)
    println("by value: " + x)
  }

  calledByValue(System.nanoTime())
  calledByName(System.nanoTime())    // This is extremely useful in lazy streams and in things that might fail

  def infinite(): Int = 1 + infinite()
  def printFirst(x: Int, y: => Int) = println(x)

  // printFirst(infinite(), 34)      CRASH !!!!!
  printFirst(34, infinite())         // NO CRASH !!!!

  // By name parameter delays, the evaluation of the expression passed here until it's used
}
