package lectures.part1basics

import scala.annotation.tailrec

object Recursion extends App {

  def factorial(n: Int): Int = {
    if (n <= 1) 1
    else {
      //println("computing factorial of " + n + " - I first need factorial of " + (n - 1))
      val result = n * factorial(n-1)
      //println("Computed factorial of " + n)

      result
    }
  }

    println(factorial(10))
    // println(factorial(1000000))   => CRASH !!!!

  def anotherFactorial(n: Int): BigInt = {
    @tailrec    // Não é necessário, mas se colocarmos assegura, em tempo de compilação, qie trata-se de TAIL RECURSION
    def factorialHelper(x: Int, acum: BigInt): BigInt =
      if (x <= 1) acum
      else factorialHelper(x-1, x * acum)  // TAIL RECURSION = use recursive call as the LAST expression

    factorialHelper(n, 1)
  }

  println(anotherFactorial(10))
  println(anotherFactorial(20000))   //   => NO CRASH !!!!

  /*
    1. Concatenate a string n times
  */

  def concatenateString(s: String, n: Int): String = {
    @tailrec
    def concHelper(concS: String, x: Int, acum: String): String = {
      if (x <= 0) acum
      else concHelper(concS, x-1, acum + concS)
    }
    concHelper(s, n, "")
  }

  println(concatenateString("casa", 3))

  /*
    2. IsPrime function tail recursive
  */

  def isPrime(n: Int): Boolean = {
    @tailrec
    def isPrimeHelper(t: Int, isStillPrime: Boolean): Boolean =
      if (t <= 1) isStillPrime
      else isPrimeHelper(t-1, isStillPrime && (n % t != 0))

    isPrimeHelper(n / 2, true)
  }
  println(isPrime(20))
  println(isPrime(37))

  /*
    3. Fibonacci function, tail recursive

    1, 1, 2, 3, 5, 8, 13
    f(1) = 1
    f(2) = 1
    f(n) = f(n-2) + f(n-1)
  */

  def fibonacci(n: Int): Int = {
    @tailrec
    def fib(prev: Int, actual: Int, count: Int): Int =
      if (n == 1 || n == 2 || count == n) actual
      else fib(actual, prev + actual, count + 1)

    fib(1, 1, 2)
  }
  println(fibonacci(7))

}


